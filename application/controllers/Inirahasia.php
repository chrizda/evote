<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/27/17
 * Time: 2:26 PM
 */

class Inirahasia extends CI_Controller{

    public function __construct(){
        parent ::__construct();
        $this->load->model('madmin');
    }

    public function regis(){
        $nim = $this->input->post('nim');
        $registerr = $this->madmin->register($nim);
        if($registerr){
            $this->session->set_flashdata('abc','NIM telah didaftarkan! Siap untuk vote');
            $this->load->view('regisadmin');
        }
        else {
            $this->session->set_flashdata('abc', 'Sudah terdaftar / Sudah vote / NIM tidak ada');
            $this->load->view('regisadmin');
        }
    }

    public function inihasilnyaamatsangatrahasia(){
        $data['hasil'] = $this->madmin->ambilhasil()->result();
        $this->load->view('hasil',$data);
    }

    public function daftarbaru(){
        $nim = $this->input->post('nimbaru');
        $daftar = $this->madmin->daftarbaru($nim);
        if($daftar){
            $this->session->set_flashdata('def','NIM telah didaftarkan!');
            $this->load->view('regisadmin');
        }
        else {
            $this->session->set_flashdata('def', 'NIM sudah terdaftar!');
            $this->load->view('regisadmin');
        }
    }

    public function loginadmin(){
        $id = $this->input->post('id');
        $pwd = $this->input->post('pwd');
        if($id === 'admin' && $pwd === 'rahasiaya'){
            $this->session->set_userdata('admin');
            $this->load->view('regisadmin');
        }
        else{
            redirect('/');
        }
    }

    function index(){
        $this->load->view('loginadmin');
    }
}