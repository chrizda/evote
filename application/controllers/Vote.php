<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vote extends CI_Controller {

    public function __construct(){
        parent ::__construct();
        $this->load->model('mlogin');
    }

    public function loginaction(){
        $id = $this->input->post('nim');
        $loginnya = $this->mlogin->logina($id);
        $arraya = array(
            'nim' => $id
        );
        if($loginnya){
            $this->load->view('vote');
            $this->session->set_userdata($arraya);
        }
        else{
            $this->session->set_flashdata('error','NIM anda belum terdaftar');
            redirect('/');
        }
    }

    public function vote($pilihan){
        $nim = $this->session->userdata('nim');
        $this->load->model('mvote');
        $this->mvote->updatepilihan($nim, $pilihan);
        $this->session->unset_userdata('nim');
        redirect('/');
    }

    public function index()
    {
        $this->load->view('login');
    }
}
