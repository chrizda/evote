<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Madmin extends CI_Model{

    function register($nim){
        $this->db->from('vote');
        $this->db->where('nim',$nim);
        $this->db->where('status',0);
        $query = $this->db->get();

        if($query->num_rows() == 1){
            $this->db->set('status',1);
            $this->db->where('nim',$nim);
            $this->db->update('vote');
            return true;
        }
        else{
            return false;
        }

    }

    function ambilhasil(){
        $query = "select count(nim) as jumlah from vote group by pilihan";
        return $this->db->query($query);
    }


    function daftarbaru($nim){
        $this->db->from('vote');
        $this->db->where('nim',$nim);
        $query = $this->db->get();

        if($query->num_rows() == 1){
            return false;
        }
        else{
            $arrayinputan = array(
                'nim' => $nim,
                'pilihan' => 0,
                'status' => 1
            );
            $this->db->insert('vote',$arrayinputan);
            return true;
        }

    }




}