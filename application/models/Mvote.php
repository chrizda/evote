<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mvote extends CI_Model{

    function updatepilihan($nim, $pilihan){
        $this->db->from('vote');
        $this->db->where('nim',$nim);
        $this->db->where('pilihan',0);
        $query = $this->db->get();

        if($query->num_rows() == 1){
            $this->db->set('status',2);
            if($pilihan==1){
                $this->db->set('pilihan',1);
            }
            elseif($pilihan==2){
                $this->db->set('pilihan',2);
            }
            $this->db->where('nim',$nim);
            $this->db->update('vote');
            return true;
        }
        else{
            return false;
        }

    }

}
