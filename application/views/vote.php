<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>E-VOTE</title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
</head>
<body>

<div class="container">
    <h4>Welcome, <?=$this->session->userdata('nim')?></h4>
</div>

<div class="container" style="padding-top:20px;">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="jumbotron text-center" style="background-color:transparent">
                <img src="<?=base_url()?>assets/img/cda.jpeg" style="max-height:250px">
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>

<div class="container" style="padding-top:20px;">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 text-center">
            <p>Apakah kamu memilih ?????? untuk menjadi Ketua BEM?</p>
        </div>
        <div class="col-md-3"></div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-4 text-center">
            <a href="<?=base_url()?>vote/vote/1" class="btn btn-success btn-lg btn-block" onclick="return confirm('Yakin?')">IYA</a>
        </div>
        <div class="col-md-4 text-center">
            <a href="<?=base_url()?>vote/vote/2" class="btn btn-danger btn-lg btn-block" onclick="return confirm('Yakin?')">TIDAK</a>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>






</body>
</html>