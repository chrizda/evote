<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>E-VOTE</title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
</head>
<body>

<div class="container" style="padding-top:20px;">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="jumbotron text-center" style="background-color:transparent">
                <h1>E-VOTE</h1>
                <p>Fakultas Kesehatan Masyarakat</p>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4 text-center">
            <font color="#8b0000">
            <?php if ($this->session->flashdata('error') != ''):
            echo $this->session->flashdata('error');
            endif;
            ?>
            </font>
            <form method="post" action="<?=base_url()?>vote/loginaction">
                <div class="form-group">
                    <input type="text" class="form-control" name="nim" placeholder="Masukkan NIM">
                </div>
                <button type="submit" class="btn btn-warning">VOTE !</button>
            </form>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>


<script src="<?php echo base_url()?>assets/js/sweetalert.min.js"></script>
</body>
</html>