<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Admin</title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
</head>
<body>

    <div class="container" style="padding-top:80px;">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <font color="#8b0000">
                    <?php if ($this->session->flashdata('abc') != ''):
                        echo $this->session->flashdata('abc');
                    endif;
                    ?>
                </font>
                <form method="post" action="<?=base_url()?>inirahasia/regis">
                    <div class="form-group">
                        <label for="nim">Daftarkan NIM untuk vote</label>
                        <input type="text" class="form-control" name="nim" placeholder="Masukkan NIM">
                    </div>
                    <button type="submit" class="btn btn-warning">Register</button>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>


    <div class="container" style="padding-top:80px;">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <font color="#8b0000">
                    <?php if ($this->session->flashdata('def') != ''):
                        echo $this->session->flashdata('def');
                    endif;
                    ?>
                </font>
                <form method="post" action="<?=base_url()?>inirahasia/daftarbaru">
                    <div class="form-group">
                        <label for="nimbaru">Daftarkan NIM yang tidak terdaftar</label>
                        <input type="text" class="form-control" name="nimbaru" placeholder="Masukkan NIM">
                    </div>
                    <button type="submit" class="btn btn-warning">Register</button>
                </form>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>






</body>
</html>