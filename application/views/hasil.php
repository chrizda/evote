<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>E-VOTE</title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <script src="<?php echo base_url();?>assets/js/jquery-1.8.3.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
</head>
<body>
<div class="container">
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <table class="table">
            <tr>
                <td class="text-center">Jumlah Ya</td>
                <td class="text-center">Jumlah Tidak</td>
            </tr>
            <tr>
                <td class="text-center"><?=$hasil[0]->jumlah?></td>
                <td class="text-center"><?=$hasil[1]->jumlah?></td>
            </tr>
        </table>
    </div>
    <div class="col-md-3"></div>
</div>
</div>



<div id="chart" style="min-width: 80vh; height: 60vh; margin-right: 20px;"></div>

</body>

<script src="<?php echo base_url();?>assets/js/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/js/exporting.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
    $(function () {

        $(document).ready(function () {

            // Build the chart
            Highcharts.chart('chart', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Hasil Perolehan Suara'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Jawaban',
                    colorByPoint: true,
                    data: [{
                        name: 'Ya',
                        y: <?=$hasil[0]->jumlah;?>
                    }, {
                        name: 'Tidak',
                        y: <?=$hasil[1]->jumlah;?>

                    }]
                }]
            });
        });

    });


</script>
</html>